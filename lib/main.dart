import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:qwil_flutter_test/redux/actions.dart';
import 'package:qwil_flutter_test/redux/epics.dart';
import 'package:redux/redux.dart';

import 'package:qwil_flutter_test/redux/reducers.dart';
import 'package:qwil_flutter_test/redux/app_state.dart';
import 'package:redux_epics/redux_epics.dart';

void main() {
  final store = new Store<AppState>(appStateReducer,
      initialState: AppState.initial(),
      middleware: [
        EpicMiddleware<AppState>(SearchEpic()),
      ]);
  runApp(new MyApp(store: store));
}

class MyApp extends StatelessWidget {
  final Store<AppState> store;

  MyApp({Key key, this.store}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return new StoreProvider<AppState>(
        store: store,
        child: new MaterialApp(
          title: 'Forever Alone',
          theme: new ThemeData(
            primarySwatch: Colors.blue,
          ),
          home: new MyHomePage(title: 'Forever Alone'),
        ));
  }
}

class MyHomePageViewModel {
  final AppState state;
  final void Function() onToggle;

  MyHomePageViewModel({this.state, this.onToggle});
}

class MyHomePage extends StatelessWidget {
  final String title;

  MyHomePage({Key key, this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, MyHomePageViewModel>(
      distinct: true,
      converter: (store) => MyHomePageViewModel(
          state: store.state,
          onToggle: () => store.dispatch(UISimulationToggle())),
      builder: (context, vm) => new Scaffold(
            appBar: new AppBar(
              title: new Text(title),
            ),
            body: new Center(
              child: new Container(
                child: new Stack(
                  children: <Widget>[
                    _mainDashboard(vm),
                    _toggleButton(vm),
                  ],
                ),
              ),
            ),
          ),
    );
  }

  Widget _mainDashboard(MyHomePageViewModel vm) {
    return new Column(
      children: <Widget>[
        _messageRow('FAKE FIRST USER', vm.state.firstUserMessage),
        _messageRow('SECOND USER', vm.state.secondUserMessage),
        _messageRow('ANOTHER USER', vm.state.thirdUserMessage),
      ],
    );
  }

  Widget _messageRow(String user, String message) {
    return Row(
      children: [
        Expanded(
          child: Column(
            children: [
              Container(
                child: new Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    user,
                  ),
                ),
              ),
              Text(
                message,
              ),
            ],
          ),
        ),
        Icon(
          Icons.access_time,
        ),
      ],
    );
  }

  Widget _toggleButton(MyHomePageViewModel vm) {
    return new RaisedButton(
      onPressed: vm.onToggle,
      textColor: Colors.white,
      color: Colors.blue,
      child: new Text(
        vm.state.isSimulating ? 'Stop' : "Start",
        style: new TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 20.0,
        ),
      ),
    );
  }
}
