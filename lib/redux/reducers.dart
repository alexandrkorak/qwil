import 'package:qwil_flutter_test/redux/actions.dart';
import 'package:qwil_flutter_test/redux/app_state.dart';
import 'package:redux/redux.dart';

final appStateReducer = combineReducers<AppState>([
  TypedReducer<AppState, UISimulationToggle>(_onSimulationToggle),
  TypedReducer<AppState, UiDisplayUsers>(_onDisplay)
]);

AppState _onSimulationToggle(AppState state, UISimulationToggle action) {
  return AppState.restore(state);
}

AppState _onDisplay(AppState state, UiDisplayUsers action) {
  return AppState.data(action.firstUser, action.secondUser, action.thirdUser);
}
