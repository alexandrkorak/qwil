import 'dart:async';
import 'dart:math';

import 'package:qwil_flutter_test/redux/actions.dart';
import 'package:qwil_flutter_test/redux/app_state.dart';
import 'package:redux_epics/redux_epics.dart';
import 'package:rxdart/rxdart.dart';

class SearchEpic implements EpicClass<AppState> {
  Stream<dynamic> call(Stream<dynamic> actions, EpicStore<AppState> epicStore) {
    return Observable(actions)
        .ofType(TypeToken<UISimulationToggle>())
        .switchMap((s) => epicStore.state.isSimulating
            ? Observable.periodic(Duration(seconds: 1), (t) => t).flatMap((s) =>
                Observable.combineLatest3(
                    _firstUserMessages(),
                    _secondUserMessages(),
                    _thirdUserMessages(),
                    (String a, String b, String c) => UiDisplayUsers(a, b, c)))
            : Observable.empty());
  }
}

Random random = Random();

/**
 * Generating random number from 1 to 2 seconds in milis.
 */
int getRandomDelay() {
  return random.nextInt(1000) + 1000;
}

Observable<String> create(String userName) {
  return Observable.just(userName)
      .switchMap((s) => Observable.periodic(
          Duration(milliseconds: getRandomDelay()),
          (i) =>
              userName +
              " " +
              i.toString() +
              " " +
              (DateTime.now().millisecond % 100).toString()))
      .distinct((prev, next) => prev == next)
      .delay(Duration(milliseconds: random.nextInt(100) > 99 ? 10000 : 0))
      .defaultIfEmpty("N/A");
}

Observable<String> _firstUserMessages() {
  return create("firstUser");
}

Observable<String> _secondUserMessages() {
  return create("secondUser");
}

Observable<String> _thirdUserMessages() {
  return create("thirdUser");
}
