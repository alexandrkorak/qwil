class AppState {
  final bool isSimulating;
  final String firstUserMessage;
  final String secondUserMessage;
  final String thirdUserMessage;

  AppState(
      {this.isSimulating = false,
      this.firstUserMessage =
          "There are many variations of passages of Lorem Ipsum available,",
      this.secondUserMessage =
          "It is a long established fact that a reader will be distracted by the",
      this.thirdUserMessage =
          "Lorem Ipsum is simply dummy text of the printing and typesetting industry."});

  factory AppState.initial() => AppState();

  factory AppState.restore(AppState prev) => AppState(
      isSimulating: !prev.isSimulating,
      firstUserMessage: prev.firstUserMessage,
      secondUserMessage: prev.secondUserMessage,
      thirdUserMessage: prev.thirdUserMessage);

  factory AppState.data(String firstUserMessage, String secondUserMessage,
          String thirdUserMessage) =>
      AppState(
          isSimulating: true,
          firstUserMessage: firstUserMessage,
          secondUserMessage: secondUserMessage,
          thirdUserMessage: thirdUserMessage);
}
